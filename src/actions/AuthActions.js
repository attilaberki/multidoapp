import firebase from '../firebase';
import { ACTYPS } from './types';
import { Txt } from '../config';
import { Tracker, Gac } from '../ga';


export const authReset = () => {
  return {
    type: ACTYPS.AUTH.RESET
  };
};

export const emailsendedReset = () => {
  return {
    type: ACTYPS.FORGOTTENPASS.MAILSENDED
  };
};

export const emailChanged = (text) => {
  return {
    type: ACTYPS.AUTH.EMAILCHANGED,
    payload: text
  };
};

export const emailErrorChanged = (text) => {
  return {
    type: ACTYPS.AUTH.EMAILERROR,
    payload: text
  };
};

export const passwordChanged = (text) => {
  return {
    type: ACTYPS.AUTH.PASSWORDCHANGED,
    payload: text
  };
};

export const passwordErrorChanged = (text) => {
  return {
    type: ACTYPS.AUTH.PASSWORDERROR,
    payload: text
  };
};

export const userLoggedIn = () => {
  return (dispatch) => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        dispatch({ type: ACTYPS.LOGIN.AUTOLOGIN, payload: true });
      } else {
        dispatch({ type: ACTYPS.LOGIN.AUTOLOGIN, payload: false });
      }
    });
  };
};

export const loginUser = ({ email, password }) => {
  return (dispatch) => {
    dispatch({ type: ACTYPS.LOGIN.PROGRESS });
    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(user => loginUserSuccess(dispatch, user))
      .catch(() => loginUserFail(dispatch));
  };
};

const loginUserFail = (dispatch) => {
  dispatch({ type: ACTYPS.LOGIN.FAIL });
  dispatch({
    type: ACTYPS.NOTIFICATION.ADD,
    payload: { text: Txt.notification.authfailed, type: 'error' }
  });
  Tracker.appAction(Gac.appact.loginfailed);
};

const loginUserSuccess = (dispatch, user) => {
  dispatch({
    type: ACTYPS.NOTIFICATION.ADD,
    payload: { text: Txt.notification.authsuccess, type: 'info' }
  });
  dispatch({ type: ACTYPS.LOGIN.SUCCESS, payload: user });
  Tracker.appAction(Gac.appact.loginsuccess);
};


export const registerUser = ({ email, password }) => {
  return (dispatch) => {
    dispatch({ type: ACTYPS.REGISTRATION.PROGRESS });
    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(user => registerUserSuccess(dispatch, user))
      .catch(() => {
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(user => {
              registerUserSuccess(dispatch, user);
              user.sendEmailVerification();
            })
            .catch(() => registerUserFail(dispatch));
      });
  };
};

const registerUserFail = (dispatch) => {
  dispatch({ type: ACTYPS.REGISTRATION.FAIL });
  dispatch({
    type: ACTYPS.NOTIFICATION.ADD,
    payload: { text: Txt.notification.regfailed, type: 'error' }
  });
  Tracker.appAction(Gac.appact.registrationfailed);
};

const registerUserSuccess = (dispatch, user) => {
  dispatch({
    type: ACTYPS.NOTIFICATION.ADD,
    payload: { text: Txt.notification.regsuccess, type: 'info' }
  });
  dispatch({ type: ACTYPS.REGISTRATION.SUCCESS, payload: user });
  Tracker.appAction(Gac.appact.registrationsuccess);
};

export const resetUserpassword = ({ email }) => {
  return (dispatch) => {
    dispatch({ type: ACTYPS.FORGOTTENPASS.PROGRESS });
    firebase.auth().sendPasswordResetEmail(email)
      .then(() => resetUserpasswordSuccess(dispatch))
      .catch(() => resetUserpasswordFail(dispatch));
  };
};

const resetUserpasswordFail = (dispatch) => {
  dispatch({ type: ACTYPS.FORGOTTENPASS.FAIL });
  dispatch({
    type: ACTYPS.NOTIFICATION.ADD,
    payload: { text: Txt.notification.resetpassfailed, type: 'error' }
  });
  Tracker.appAction(Gac.appact.passwordresetfailed);
};

const resetUserpasswordSuccess = (dispatch) => {
  dispatch({
    type: ACTYPS.NOTIFICATION.ADD,
    payload: { text: Txt.notification.resetpassuccess, type: 'info' }
  });
  dispatch({ type: ACTYPS.FORGOTTENPASS.SUCCESS });
  Tracker.appAction(Gac.appact.passwordresetsuccess);
};
