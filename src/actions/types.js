export const ACTYPS = {
  AUTH: {
    EMAILCHANGED: 'action-auth-email-changed',
    EMAILERROR: 'action-auth-email-error',
    PASSWORDCHANGED: 'action-auth-password-changed',
    PASSWORDERROR: 'action-auth-password-error',
    RESET: 'action-auth-reset',
  },
  LOGIN: {
    AUTOLOGIN: 'action-login-autologin',
    SUCCESS: 'action-login-success',
    FAIL: 'action-login-fail',
    PROGRESS: 'action-login-progress'
  },
  REGISTRATION: {
    AUTOLOGIN: 'action-registration-autologin',
    SUCCESS: 'action-registration-success',
    FAIL: 'action-registration-fail',
    PROGRESS: 'action-registration-progress'
  },
  FORGOTTENPASS: {
    SUCCESS: 'action-forgottenpass-success',
    FAIL: 'action-forgottenpass-fail',
    PROGRESS: 'action-forgottenpass-progress',
    MAILSENDED: 'action-forgottenpass-mailsended',
  },
  ENV: {
    SET: 'action-env-set',
    KEYBOARD: 'action-env-keyboard'
  },
  DATES: {
    SETODAY: 'action-dates-settoday',
    SETCURRENT: 'action-dates-setcurrent',
    SETAVAILABLE: 'action-dates-setavailable',
    SETAVAILABLETOCURRENT: 'action-dates-setavailabletocurrent',
    SETANIM: 'action-dates-setanim',
    JUMP: 'action-dates-jump',
    JUMPEND: 'action-dates-jump-end',
    RESET: 'action-dates-reset'
  },
  NOTIFICATION: {
    ADD: 'action-notification-add',
    REMOVE: 'action-notification-remove',
  },
  TODO: {
    FETCH: 'action-todo-fetch',
    FETCHSUCCESS: 'action-todo-fetch-success',
    ADD: 'action-todo-add',
    SORT: 'action-todo-sort',
    CHANGE: 'action-todo-change',
    MOVE: 'action-todo-move',
    DELETE: 'action-todo-delete',
    RESET: 'action-todo-reset',
    SETINIT: 'action-todo-set-init',
    SETTINGS: 'action-todo-settings',
    VISITED: 'action-todo-visited',
  },
  TASK: {
    DATECHANGED: 'action-task-date-changed',
    TITLECHANGED: 'action-task-title-changed',
    TITLEERROR: 'action-task-title-error',
    SETDATE: 'action-task-set-date',
    SETFORM: 'action-task-set-form',
    RESETFORM: 'action-task-reset-form',
    SORTABLELIST: 'action-task-sortable-list',
    SETSUM: 'action-task-set-sum'
  },
  MODAL: {
    LOGOUTSET: 'action-modal-logout-set',
    SETTINGSSET: 'action-modal-settings-set',
    HOWTOSET: 'action-modal-howto-set',
    HOWTOTYPE: 'action-modal-howto-type',
    MENUSET: 'action-modal-menu-set',
    TASKFORMSET: 'action-modal-taskform-set',
    TASKFORMTYPESET: 'action-modal-taskform-type-set',
    CURTAINSET: 'action-modal-curtain-set'
  },
  CTXMENU: {
    SHOW: 'action-ctxmenu-show',
    HIDE: 'action-ctxmenu-hide'
  }
};
