import React, { Component } from 'react';
import { Platform, View, KeyboardAvoidingView, Animated, Easing, Keyboard } from 'react-native';
import { connect } from 'react-redux';
import { Def, Txt } from '../../config';
import { Tracker, Gac } from '../../ga';
import Login from './Login';
import Registration from './Registration';
import Forgotten from './Forgotten';

import {
  emailChanged,
  passwordChanged,
  emailErrorChanged,
  passwordErrorChanged,
  loginUser,
  todoSetinit,
  authReset,
  registerUser,
  resetUserpassword,
  emailsendedReset
} from '../../actions';


class AuthForms extends Component {

  state = {
    spinValue: new Animated.Value(0),
    opacity: new Animated.Value(1),
    animDuration: 500,
    animBounce: 1.5,
    perspective: 1000,
    perspectiveportrait: 1300,
    animout: false,
    animforward: true,
    mode: 'login',
    types: {
      login: 'login',
      registration: 'registration',
      forgottenpass: 'forgottenpass'
    }
  }

  componentWillMount() {
    this.props.todoSetinit();
  }

  componentDidMount() {
    Tracker.screenView(Gac.scr.login);
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.resetmailsended) {
      this.props.emailsendedReset();
      this.onPressLoginMode();
    }
  }

  onEmailChange(text) { this.props.emailChanged(text); }
  onPasswordChange(text) { this.props.passwordChanged(text); }

  onLoginPress() {
    const { email, password } = this.props;
    if (this.validateForm()) { this.props.loginUser({ email, password }); }
    Tracker.userAction(Gac.usract.formlogin);
  }

  onRegistrationPress() {
    const { email, password } = this.props;
    if (this.validateForm()) { this.props.registerUser({ email, password }); }
    Tracker.userAction(Gac.usract.formregistration);
  }

  onPasswordResetPress() {
    const { email } = this.props;
    if (this.validateForm(false)) { this.props.resetUserpassword({ email }); }
    Tracker.userAction(Gac.usract.formforgottenpass);
  }

  onSubmitEditing() {
    switch (this.state.mode) {
      case this.state.types.registration:
        this.onRegistrationPress(); break;
      case this.state.types.login:
        this.onLoginPress(); break;
      case this.state.types.forgottenpass:
        this.onPasswordResetPress(); break;
      default: break;
    }
  }

  onPressRegMode() {
    this.setState({ animforward: true });
    this.rotateTo(this.state.types.registration);
  }

  onForgottenMode() {
    this.setState({ animforward: false });
    this.rotateTo(this.state.types.forgottenpass);
  }

  onPressPasswordCancel() {
    this.setState({ animforward: true });
    this.rotateTo(this.state.types.login);
  }

  onPressLoginMode() {
    this.setState({ animforward: false });
    this.rotateTo(this.state.types.login);
  }

  validateForm(all = true) {
    const { email, password } = this.props;
    let formIsValid = true;
    this.props.emailErrorChanged('');
    this.props.passwordErrorChanged('');

    if (!this.validateEmail(email)) {
      this.props.emailErrorChanged(Txt.login.emailerror);
      formIsValid = false;
    }

    if (all && password.length < 6) {
      this.props.passwordErrorChanged(Txt.login.passerror);
      formIsValid = false;
    }

    return formIsValid;
  }

  validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  rotateTo(mode) {
    if (Platform.OS !== 'web') { Keyboard.dismiss(); }

    Animated.parallel([
      Animated.timing(this.state.spinValue, { toValue: 1, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration }),
      Animated.timing(this.state.opacity, { toValue: 0, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration }),
    ]).start(() => {
      this.setState({ mode, animout: true  });
      this.props.authReset();
      Animated.parallel([
        Animated.timing(this.state.spinValue, { toValue: 0, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration }),
        Animated.timing(this.state.opacity, { toValue: 1, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration }),
      ]).start(() => {
        this.setState({ animout: false  });
      });
    });
  }

  renderRotation() {
    const aout = this.state.animforward ? ['0deg', '-90deg'] : ['-0deg', '90deg'];
    const ain = this.state.animforward ? ['0deg', '90deg'] : ['0deg', '-90deg'];
    const spin = this.state.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: this.state.animout ? aout : ain
    });

    return { opacity: this.state.opacity, transform: [{ perspective: this.props.env.wide ? this.state.perspective : this.state.perspectiveportrait }, { rotateY: spin }] };
  }

  renderForm() {
    if (this.state.mode === this.state.types.registration) {
      return (
        <Registration
          loading={this.props.loading}
          rotation={this.renderRotation()}
          emailChangeText={this.onEmailChange.bind(this)}
          emailValue={this.props.email}
          emailError={this.props.emailError}
          passwordChangeText={this.onPasswordChange.bind(this)}
          passwordValue={this.props.password}
          passwordError={this.props.passwordError}
          onSubmitEditing={this.onSubmitEditing.bind(this)}
          type={this.state.types.registration}
          pressButton={this.onRegistrationPress.bind(this)}
          pressRight={this.onPressLoginMode.bind(this)}
        />
      );
    }
    if (this.state.mode === this.state.types.forgottenpass) {
      return (
        <Forgotten
          loading={this.props.loading}
          rotation={this.renderRotation()}
          emailChangeText={this.onEmailChange.bind(this)}
          emailValue={this.props.email}
          emailError={this.props.emailError}
          onSubmitEditing={this.onSubmitEditing.bind(this)}
          type={this.state.types.forgottenpass}
          pressButton={this.onPasswordResetPress.bind(this)}
          pressRight={this.onPressPasswordCancel.bind(this)}
        />
      );
    }
    return (
      <Login
        loading={this.props.loading}
        rotation={this.renderRotation()}
        emailChangeText={this.onEmailChange.bind(this)}
        emailValue={this.props.email}
        emailError={this.props.emailError}
        passwordChangeText={this.onPasswordChange.bind(this)}
        passwordValue={this.props.password}
        passwordError={this.props.passwordError}
        onSubmitEditing={this.onSubmitEditing.bind(this)}
        type={this.state.types.login}
        pressButton={this.onLoginPress.bind(this)}
        pressLeft={this.onForgottenMode.bind(this)}
        pressRight={this.onPressRegMode.bind(this)}
      />
    );
  }

  renderWrapper() {
    if (this.props.env.wide && this.props.env.isdesktop) {
      return (
        <View style={styles.wrapper} className="auth-wrapper">
          {this.renderForm()}
        </View>
      );
    } else if (this.props.env.wide && this.props.env.ismobile) {
      return (
        <View style={styles.KAVview}>
          <KeyboardAvoidingView behavior="height" style={styles.KAVcontainer}>
            <View style={styles.wrapper}>
              {this.renderForm()}
            </View>
          </KeyboardAvoidingView>
        </View>
      );
    }

    return (
      <View style={styles.KAVview}>
        <KeyboardAvoidingView behavior="height" style={styles.KAVcontainer}>
          {this.renderForm()}
        </KeyboardAvoidingView>
      </View>
    );
  }

  render() {
    return this.renderWrapper();
  }
}

const styles = {
  KAVview: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
  },
  KAVcontainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  wrapper: {
    alignSelf: 'center',
    width: 400,
    height: 400,
    backgroundColor: Def.colorBlue,
  },
  container: {
    paddingTop: (Platform.OS === 'ios') ? 42 : 32,
    paddingLeft: 20,
    paddingRight: 20,
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    alignSelf: Def.wide ? 'stretch' : 'flex-start',
  },
};

const mapStateToProps = ({ auth, env, todo }) => {
  const { email, password, error, loading, emailError, passwordError, resetmailsended } = auth;
  return { email, password, error, loading, emailError, passwordError, resetmailsended, env, todo };
};

export default connect(mapStateToProps, {
  emailChanged,
  passwordChanged,
  loginUser,
  emailErrorChanged,
  passwordErrorChanged,
  todoSetinit,
  authReset,
  registerUser,
  resetUserpassword,
  emailsendedReset
})(AuthForms);
