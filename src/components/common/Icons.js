import React from 'react';
import { View, Text, Platform } from 'react-native';
import { Def } from '../../config';

const Icons = (props) => {
  const { src, fill, today } = props;
  const nowcolor = today ? Def.colorBlue : Def.colorIcon;
  const color = fill || Def.colorIcon;
  switch (src) {
    case 'pipe': return (<View className="iconfont pipe" style={styles.view}><Text style={[styles.iconfont, styles.iconPipe, { color }]}>j</Text></View>);
    case 'sort': return (<View className="iconfont sort" style={styles.view}><Text style={[styles.iconfont, styles.iconSort, { color }]}>s</Text></View>);
    case 'eye': return (<View className="iconfont eye" style={styles.view}><Text style={[styles.iconfont, { color }]}>e</Text></View>);
    case 'search': return (<View className="iconfont search" style={styles.view}><Text style={[styles.iconfont, { color }]}>o</Text></View>);
    case 'settings': return (<View className="iconfont settings" style={styles.view}><Text style={[styles.iconfont, styles.iconSettings, { color }]}>r</Text></View>);
    case 'bulb': return (<View className="iconfont bulb" style={styles.view}><Text style={[styles.iconfont, styles.iconBulb, { color }]}>p</Text></View>);
    case 'info': return (<View className="iconfont info" style={styles.view}><Text style={[styles.iconfont, { color }]}>i</Text></View>);
    case 'logout': return (<View className="iconfont logout" style={styles.view}><Text style={[styles.iconfont, styles.iconLogout, { color }]}>q</Text></View>);
    case 'now': return (<View className="iconfont now" style={styles.view}><Text style={[styles.iconfont, { color }]}>t</Text></View>);
    case 'day': return (<View className="iconfont day" style={styles.view}><Text style={[styles.iconfont, styles.iconDay, { color }]}>#</Text></View>);
    case 'right': return (<View className="iconfont right" style={styles.view}><Text style={[styles.iconfont, { color }]}>></Text></View>);
    case 'left': return (<View className="iconfont left" style={styles.view}><Text style={[styles.iconfont, { color }]}>{'<'}</Text></View>);
    case 'close': return (<View className="iconfont close" style={styles.view}><Text style={[styles.iconfont, styles.iconClose, { color }]}>x</Text></View>);
    case 'plus': return (<View className="iconfont plus" style={styles.view}><Text style={[styles.iconfont, { color }]}>+</Text></View>);
    case 'menu': return (<View className="iconfont menu" style={styles.view}><Text style={[styles.iconfont, { color }]}>=</Text></View>);
    case 'bin': return (<View className="iconfont bin" style={styles.view}><Text style={[styles.iconfont, { color }]}>b</Text></View>);
    case 'calendar': return (<View className="iconfont calendar" style={styles.view}><Text style={[styles.iconfont, { color }]}>a</Text></View>);
    case 'ring': return (<View className="iconfont ring" style={styles.view}><Text style={[styles.iconfont, styles.iconRing, { color: nowcolor }]}>c</Text></View>);
    case 'circle': return (<View className="iconfont circle" style={styles.view}><Text style={[styles.iconfont, { color }]}>l</Text></View>);
    case 'here': return (<View className="iconfont here" style={styles.view}><Text style={[styles.iconfont, { color }]}>d</Text></View>);
    case 'rect': return (<View className="iconfont rect" style={styles.view}><Text style={[styles.iconfont, { color }]}>m</Text></View>);
    case 'stop': return (<View className="iconfont stop" style={styles.view}><Text style={[styles.iconfont, { color }]}>n</Text></View>);
    case 'pen': return (<View className="iconfont pen" style={styles.view}><Text style={[styles.iconfont, styles.iconPen, { color }]}>h</Text></View>);
    case 'pencil': return (<View className="iconfont pencil" style={styles.view}><Text style={[styles.iconfont, styles.iconPen, { color }]}>k</Text></View>);
    default: return null;
  }
};

const styles = {
  view: {
    height: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconfont: {
    height: 21,
    fontFamily: 'multido',
    fontSize: 26,
    textAlign: 'center',
    textAlignVertical: 'top',
  },
  iconClose: {
    paddingTop: 2,
    fontSize: 24,
  },
  iconSort: {
    height: 15,
    fontSize: 22,
  },
  iconPipe: {
    paddingTop: 1
  },
  iconRing: {
    paddingTop: (Platform.OS === 'web') ? 1 : 1.5,
    fontSize: 24,
  },
  iconDay: {
    paddingTop: (Platform.OS === 'web') ? 2 : 1,
    fontSize: (Platform.OS === 'web') ? 23 : 24,
  },
  iconLogout: {
    paddingTop: (Platform.OS === 'web') ? 5 : 2,
    fontSize: (Platform.OS === 'web') ? 23 : 24,
    marginLeft: -3,
    marginRight: 5,
  },
  iconSettings: {
    paddingTop: (Platform.OS === 'web') ? 3 : 2,
    fontSize: 24,
  },
  iconBulb: {
    paddingTop: (Platform.OS === 'web') ? 0 : 1,
    marginLeft: -2,
    marginRight: 1,
  },
  iconPen: {
    fontSize: 29,
  }
};

export default Icons;
