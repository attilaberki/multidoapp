import React, { Component } from 'react';
import { Text, View, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { Def, Txt } from '../../config';
import Icon from './Icons';


class InputDate extends Component {

  state = {
    date: null,
    dayofweek: null,
    day: null,
    month: null,
    year: null,
  }

  componentWillMount() {
    this.setDateDetails(this.props.date);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.date !== nextProps.date) return true;
    if (this.state.date !== nextState.date) return true;
    return false;
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.props.date !== nextProps.date) {
      this.setDateDetails(nextProps.date);
    }
  }

  setDateDetails(date) {
    const newdate = new Date(date);
    this.setState({
      date: newdate,
      dayofweek: Txt.calendar.weekday[newdate.getDay()],
      day: (newdate.getDate() < 10) ? `0${newdate.getDate()}` : newdate.getDate(),
      month: Txt.calendar.monthnames[newdate.getMonth()],
      year: newdate.getFullYear(),
    });
  }

  pressIndicator() {
    this.props.onPress();
    Keyboard.dismiss();
  }

  render() {
    return (
        <View style={styles.cont}>
          <View style={this.props.editable ? styles.datecont : [styles.datecont, styles.datecontHidden]}>
            <View style={styles.daynamcont}>
              <Text style={styles.daynametext}>{this.state.dayofweek}</Text>
            </View>
            <View style={styles.othercont}>
              <View style={styles.date}>
                <Text style={this.props.editable ? styles.datetext : [styles.datetext, styles.datetextHidden]}>{this.state.month}</Text>
                <Text style={this.props.editable ? styles.datetext : [styles.datetext, styles.datetextHidden]}>{this.state.day},</Text>
                <Text style={this.props.editable ? styles.datetext : [styles.datetext, styles.datetextHidden]}>{this.state.year}</Text>
              </View>
              <View style={this.props.editable ? styles.dayicon : styles.dayiconHidden}>
                <Icon src="day" fill="#000000" />
              </View>
            </View>
          </View>
          <View style={styles.pickercont}>
            <TouchableWithoutFeedback onPress={this.pressIndicator.bind(this)}><View style={this.props.editable ? styles.indicator : styles.indicatorHidden} /></TouchableWithoutFeedback>
          </View>
        </View>


    );
  }
}

const styles = {
  cont: {
    flex: 1,
    marginTop: 10
  },
  pickercont: {
    alignSelf: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'stretch',
    position: 'absolute',
    opacity: 1,
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
  custom: {
    width: 0,
    height: 0,
    overflow: 'hidden',
    opacity: 0,
  },
  indicator: {
    flex: 1,
    opacity: 0,
  },
  indicatorHidden: {
    width: 0,
    height: 0,
    overflow: 'hidden',
    opacity: 0,
  },
  datecont: {
    flexDirection: 'column',
    alignItems: 'stretch',
    borderBottomColor: Def.colorGray,
    borderBottomWidth: 1
  },
  datecontHidden: {
    borderBottomWidth: 0
  },
  daynamcont: {
    marginBottom: 10,
  },
  daynametext: {
    fontFamily: Def.fontExtLight,
    color: Def.colorBlue,
    fontSize: 36,
  },
  daynametextHidden: {
    color: Def.colorTextGray,
  },
  othercont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    paddingBottom: 12,
  },
  date: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
  },
  datetext: {
    fontFamily: Def.fontExtLight,
    color: Def.colorBlack,
    fontSize: 28,
    marginRight: 10
  },
  datetextHidden: {
    color: Def.colorTextGray,
  },
  dayicon: {
    paddingBottom: 4,
  },
  dayiconHidden: {
    width: 0,
    height: 0,
    overflow: 'hidden',
    opacity: 0,
  }
};

export default InputDate;
