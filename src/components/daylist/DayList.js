import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Animated } from 'react-native';
import { WindowResizeListener } from 'react-window-resize-listener';
import { dateSetCurrent, dateSetAvailable, dateSetAvailableToCurrent, dateJumpEnd, dateSetAnim } from '../../actions';
import { Def, Func } from '../../config';
import DayPanel from './DayPanel';
import Footer from './Footer';
import CtxMenu from './CtxMenu';
import { Tracker, Gac } from '../../ga';

class DayList extends Component {
  state = {
    translateX: new Animated.Value(0),
    opacity: new Animated.Value(1),
    windowSize: null,
    clickable: true,
    jumping: null
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.date.current !== nextProps.date.current) { return true; }
    if (this.props.date.jump !== nextProps.date.jump) { return true; }
    if (this.state.jumping !== nextState.jumping) { return true; }
    if (this.state.windowSize !== nextState.windowSize) { return true; }
    return false;
  }

  componentWillUpdate(nextProps, nextState) {
    const { jump, current } = nextProps.date;
    if (this.props.date.jump !== jump) {
      if (Func.getDayDiffAbs(current, jump) > 2) {
        if (Func.getDayDiff(current, jump) < 0) { this.jumpToDate(jump, 'prev'); } else { this.jumpToDate(jump, 'next'); }
      } else if (Func.getDayDiffAbs(current, jump) === 2) {
        if (Func.getDayDiff(current, jump) < 0) { this.moveToDate(jump, 'prev'); } else { this.moveToDate(jump, 'next'); }
      } else {
        if (Func.getDayDiff(current, jump) < 0) { this.pressPrev(); } else { this.pressNext(); }
      }
    }

    if (this.state.jumping !== nextState.jumping) {
      if (nextState.jumping !== null) { this.jumpEnd(nextState.jumping); }
    }
  }

  jumpToDate(date, dir) {
    if (this.state.clickable) {
      this.setState({ clickable: false });
      this.props.dateSetAnim(true);
      const moveto = (dir === 'prev') ? Def.wideDaysWidth : -Def.wideDaysWidth;
      Animated.parallel([
        Animated.timing(this.state.opacity, { toValue: 0, duration: Def.jumpDuration, }),
        Animated.timing(this.state.translateX, { toValue: moveto, duration: Def.jumpDuration, }),
      ]).start(() => {
        this.props.dateSetAvailable(date);
        this.props.dateSetAvailableToCurrent();
        this.setState({ jumping: dir });
      });
    }
  }

  moveToDate(date, dir) {
    if (this.state.clickable) {
      this.setState({ clickable: false });
      this.props.dateSetAnim(true);
      this.props.dateSetAvailable(date);
      const moveto = (dir === 'prev') ? (Def.wideDaysWidth * 2) : -(Def.wideDaysWidth * 2);
      Animated.timing(this.state.translateX, { toValue: moveto, duration: (Def.listDuration), }).start(() => {
        this.props.dateSetAvailableToCurrent();
        this.props.dateJumpEnd();
        this.state.translateX.setValue(0);
        this.props.dateSetAnim(false);
        this.setState({ clickable: true });
      });
    }
  }

  jumpEnd(jumping) {
    const moveto = (jumping === 'prev') ? -Def.wideDaysWidth : Def.wideDaysWidth;
    this.state.translateX.setValue(moveto);
    Animated.parallel([
      Animated.timing(this.state.opacity, { toValue: 1, duration: Def.jumpDuration, }),
      Animated.timing(this.state.translateX, { toValue: 0, duration: Def.jumpDuration, }),
    ]).start(() => {
      this.props.dateJumpEnd();
      this.props.dateSetAnim(false);
      this.setState({ jumping: null, clickable: true });
    });
  }

  pressPrev() {
    if (this.state.clickable) {
      this.setState({ clickable: false });
      this.props.dateSetAnim(true);
      this.props.dateSetAvailable(Func.getDay(-1, this.props.date.current));
      Animated.timing(this.state.translateX, { toValue: Def.wideDaysWidth, duration: Def.listDuration, }).start(() => {
        this.props.dateSetAvailableToCurrent();
        this.props.dateJumpEnd();
        this.props.dateSetAnim(false);
        this.state.translateX.setValue(0);
        this.setState({ clickable: true });
      });
      Tracker.userAction(Gac.usract.footprev);
    }
  }

  pressNext() {
    if (this.state.clickable) {
      this.setState({ clickable: false });
      this.props.dateSetAnim(true);
      this.props.dateSetAvailable(Func.getDay(1, this.props.date.current));
      Animated.timing(this.state.translateX, { toValue: -Def.wideDaysWidth, duration: Def.listDuration, }).start(() => {
        this.props.dateSetAvailableToCurrent();
        this.props.dateJumpEnd();
        this.props.dateSetAnim(false);
        this.state.translateX.setValue(0);
        this.setState({ clickable: true });
      });
      Tracker.userAction(Gac.usract.footnext);
    }
  }

  generateDays() {
    const { current } = this.props.date;
    let pnum = 1;
    if (this.props.env.wide) { pnum = 2; }
    if (this.props.env.isdesktop) { pnum = 4; }
    const dates = [];

    for (let i = -pnum; i < (pnum + 1); i++) {
      const d = Func.getDay(i, current);
      dates.push(d);
    }

    return dates.map((date) =>
      <DayPanel day={date} key={date} windowSize={this.state.windowSize} />
    );
  }

  onResize(e) {
    this.setState({ windowSize: e });
  }

  renderDays() {
    const { translateX, opacity } = this.state;
    const animStyle = { transform: [{ translateX }] };

    return (
      <View style={styles.container}>
        <WindowResizeListener onResize={this.onResize.bind(this)} />
        <CtxMenu windowSize={this.state.windowSize} />
        <View style={styles.days} className="dayListDays">
          <Animated.View style={[styles.animated, animStyle, { opacity }]} className="dayListDaysAnimated">

            {this.generateDays()}

          </Animated.View>
        </View>
        <Footer pressNext={this.pressNext.bind(this)} pressPrev={this.pressPrev.bind(this)} pressMenu={this.props.showMenu} />
      </View>
    );
  }

  render() {
    return this.renderDays();
  }
}

const styles = {
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  days: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingTop: Def.paddingTop,
  },
  animated: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'stretch',
  }
};

const mapStateToProps = ({ env, date }) => {
  return { env, date };
};


export default connect(mapStateToProps, {
  dateSetCurrent, dateSetAvailable, dateSetAvailableToCurrent, dateJumpEnd, dateSetAnim
})(DayList);
