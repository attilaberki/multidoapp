import React, { Component } from 'react';
import DayPanelMobile from './DayPanelMobile';

class DayPanel extends Component {
  render() {
    return <DayPanelMobile {...this.props} />;
  }
}

export default DayPanel;
