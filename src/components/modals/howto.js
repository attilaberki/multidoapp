import React, { Component } from 'react';
import { View, Animated, Easing, Platform } from 'react-native';
import { connect } from 'react-redux';
import { modalHowtoSet, modalHowtoTypeSet } from '../../actions';
import Modaler from './Modaler';
import { TextButton, Carousel } from '../common';
import { Def, Txt } from '../../config';
import { Tracker, Gac } from '../../ga';

class Howto extends Component {

  state = {
    appeard: false,
    opacity: new Animated.Value(1),
    bottom: new Animated.Value(2000),
    animDuration: Def.modalDuration,
    animBounce: 0.8,
    carouselanim: false,
  };

  componentWillMount() {
    if (this.props.visible) {
      Tracker.screenView(Gac.scr.howto);
      this.animIn(this.props);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.visible !== nextProps.visible) return true;
    if (this.props.firsttime !== nextProps.firsttime) return true;
    if (this.state.carouselanim !== nextState.carouselanim) return true;
    return false;
  }

  componentWillUpdate(nextProps, nextState) {
    if (!this.props.visible && nextProps.visible) {
      Tracker.screenView(Gac.scr.howto);
      this.setState({ carouselanim: false });
      this.animIn(nextProps);
    }
  }

  animIn({ firsttime }) {
    if (firsttime) {
      this.state.bottom.setValue(0);
      this.state.opacity.setValue(0);
      Animated.sequence([
        Animated.delay(100),
        Animated.timing(this.state.opacity, { toValue: 1, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration })
      ]).start(() => {
        this.setState({ carouselanim: true });
      });
    } else {
      Animated.sequence([
        Animated.delay(10),
        Animated.timing(this.state.bottom, { toValue: 0, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration })
      ]).start(() => {
        this.setState({ carouselanim: true });
      });
    }
  }

  onCancelPress() {
    this.setState({ carouselanim: false });
    Animated.timing(this.state.bottom, { toValue: 2000, duration: this.state.animDuration }).start(() => {
      this.props.modalHowtoTypeSet(false);
      this.props.modalHowtoSet(false);
    });
    Tracker.userAction(Gac.usract.howtogoback);
  }

  slides() {
    if (Platform.OS === 'web') {
      return Txt.howto.desktop;
    }

    if (this.props.env.wide) {
      return Txt.howto.mobile.filter(slide => { return slide.tablet; });
    }

    return Txt.howto.mobile;
  }

  renderForm() {
    return (
      <View style={styles.container}>
        <View style={styles.carouselCont}>
          <Carousel
            items={this.slides()}
            animation={this.state.carouselanim}
            endofFirst={this.onCancelPress.bind(this)}
            firsttime={this.props.firsttime}
          />
        </View>
        <View style={[styles.section, styles.foot]}>
          <TextButton onPress={this.onCancelPress.bind(this)} color="#BDBDBD">{this.props.firsttime ? Txt.howto.skip : Txt.howto.cancel}</TextButton>
        </View>
      </View>
    );
  }

  renderWrapperStyle() {
    const { bottom, opacity } = this.state;
    return [styles.wrapper, { bottom, opacity }];
  }

  renderWrapper() {
    if (this.props.env.wide) {
      return (
        <Animated.View style={this.renderWrapperStyle()}>
          <View style={styles.contentcont}>
            <View style={styles.box}>
              {this.renderForm()}
            </View>
          </View>
        </Animated.View>
      );
    }

    return (
      <Animated.View style={this.renderWrapperStyle()}>
        <View style={styles.contentcont}>
          {this.renderForm()}
        </View>
      </Animated.View>
    );
  }

  render() {
    return (
      <Modaler {...this.props} onRequestClose={this.onCancelPress.bind(this)}>
        <View style={styles.inmodaler}>
          {this.renderWrapper()}
        </View>
      </Modaler>
    );
  }
}


const styles = {
  inmodaler: {
    flex: 1, // is important
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  wrapper: {
    position: 'absolute',
    zIndex: 0,
    opacity: 1,
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    overflow: 'hidden',
    backgroundColor: Def.colorGray
  },
  contentcont: {
    flex: 1,
    justifyContent: 'center',
  },
  box: {
    alignSelf: 'center',
    width: 400,
    height: 600,
    marginTop: 50,
  },
  container: {
    paddingTop: 50,
    paddingLeft: 20,
    paddingRight: 20,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  section: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  head: {
    paddingBottom: 30
  },
  foot: {
    marginTop: 50,
  },

  carouselCont: {
    flex: 1,
  }

};


const mapStateToProps = ({ env, todo }) => {
  return { env, todo };
};

export default connect(mapStateToProps, { modalHowtoSet, modalHowtoTypeSet })(Howto);
