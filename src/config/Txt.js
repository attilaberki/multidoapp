import React from 'react';
import { Text, Platform } from 'react-native';
import { Def } from './';

const pfx = '';

const Txt = {
  login: {
    title: `${pfx}Please log in <br/>to your account`,
    email: `${pfx}Email`,
    password: `${pfx}Password`,
    button: `${pfx}LOGIN`,
    link: `${pfx}QUICK REGISTRATION`,
    forgotten: `${pfx}Forgotten password`,
    emailerror: `${pfx}The email address is badly formatted`,
    passerror: `${pfx}The password must be at least 6 characters`,
  },
  registration: {
    title: `${pfx}Create registration <br/>to Multido`,
    email: `${pfx}Email`,
    password: `${pfx}Password`,
    button: `${pfx}REGISTRATION`,
    link: `${pfx}GO BACK TO LOGIN`,
    emailerror: `${pfx}The email address is badly formatted`,
    passerror: `${pfx}The password must be at least 6 characters`,
  },
  forgottenpass: {
    title: `${pfx}Change password <br/>with e-mail confirmation`,
    email: `${pfx}Email`,
    button: `${pfx}SEND`,
    link: `${pfx}GO BACK TO LOGIN`,
    emailerror: `${pfx}The email address is badly formatted`,
  },
  logout: {
    title: `${pfx}Are you sure <br/>you want to log out?`,
    button: `${pfx}YES, I WANT TO LOG OUT`,
    cancel: `${pfx}Cancel`,
  },
  settings: {
    daylabel: `${pfx}What is the first weekday <br/>at your location?`,
    correctlabel: `${pfx}Do you use the<br/>auto-correction feature?`,
    correctsublabel: `${pfx}Only iOS and Android`,
    cancel: `${pfx}Go Back`,
  },
  howto: {
    cancel: `${pfx}Go Back`,
    skip: `${pfx}Skip`,
    continue: `${pfx}CONTINUE`,
    next: `${pfx}NEXT`,
    url: `${pfx}http://multido.net`,
    link: `${pfx}multido.net`,
    mobile: [
      {
        tablet: true,
        img: 'howtoHello',
        title: 'Hello',
        lead: <Text>
          <Text>Multido is a multiplatform todo application. It can be used on </Text>
          <Text style={{ fontFamily: Def.fontMedium, }}>Mobile, Desktop</Text>
          <Text> and</Text>
          <Text style={{ fontFamily: Def.fontMedium, }}> Web </Text>
          <Text>environment. More info:</Text>
        </Text>,
      },
      {
        tablet: false,
        img: 'howtoToday',
        title: 'Today',
        lead: <Text>
          <Text>If you tap the </Text>
          <Text style={{ fontFamily: Def.fontMedium, }}>circle icon</Text>
          <Text> on the bar, you can always go back to the today list.</Text>
        </Text>,
      },
      {
        tablet: true,
        img: 'howtoDelete',
        title: 'Delete',
        lead: <Text>
          <Text style={{ fontFamily: Def.fontMedium, }}>Pull right</Text>
          <Text> the row you want to delete and tap the trash icon.</Text>
        </Text>,
      },
      {
        tablet: true,
        img: 'howtoSorting',
        title: 'Sorting',
        lead: <Text>
          <Text style={{ fontFamily: Def.fontMedium, }}>Pull left</Text>
          <Text> a row than move it by the handler icon on the right. When you are finished slide back the handler.</Text>
        </Text>,
      },
      {
        tablet: true,
        img: 'howtoEdit',
        title: 'Edit',
        lead: <Text>
          <Text style={{ fontFamily: Def.fontMedium, }}>Double tap</Text>
          <Text> the row you want to modify to open the Edit task screen.</Text>
        </Text>,
      },

    ],
    desktop: [
      {
        img: 'howtoHello',
        title: 'Hello',
        lead: <Text>
          <Text>Multido is a multiplatform todo application. It can be used on </Text>
          <Text style={{ fontFamily: Def.fontMedium, }}>Mobile, Desktop</Text>
          <Text> and</Text>
          <Text style={{ fontFamily: Def.fontMedium, }}> Web </Text>
          <Text>environment. More info:</Text>
        </Text>,
      },
      {
        img: 'howtoDeleteEdit',
        title: 'Delete / Edit',
        lead: <Text>
          <Text>Use </Text>
          <Text style={{ fontFamily: Def.fontMedium, }}>right-click</Text>
          <Text> on your mouse for the context menu.</Text>
        </Text>,
      },
      {
        img: 'howtoQuickEdit',
        title: 'Quick edit',
        lead: <Text>
          <Text style={{ fontFamily: Def.fontMedium, }}>Double click</Text>
          <Text> the row you want to modify, then press enter.</Text>
        </Text>,
      },
      {
        img: 'howtoSortingMoving',
        title: 'Sorting / Moving',
        lead: <Text>
          <Text style={{ fontFamily: Def.fontMedium, }}>Drag</Text>
          <Text> the row if you want to sort your list or move the selected row to another date.</Text>
        </Text>,
      },
    ]
  },
  notification: {
    authfailed: `${pfx}Authentication Failed`,
    authsuccess: `${pfx}Authentication Successful`,
    regfailed: `${pfx}Registration Failed`,
    regsuccess: `${pfx}Registration Successful`,
    resetpassfailed: `${pfx}Email address not found`,
    resetpassuccess: `${pfx}Email sent`,
    nonet: `${pfx}Connection not available`,
    yesnet: `${pfx}Connection available`,
  },
  menu: {
    yesterday: `${pfx}Yesterday`,
    today: `${pfx}Today`,
    tomorrow: `${pfx}Tomorrow`,
    logout: `${pfx}Log out`,
    settings: `${pfx}Settings`,
    howto: `${pfx}Guide`,
    info: `${pfx}Info`,
    closemenu: `${pfx}Close menu`,
  },
  ctx: {
    edit: `${pfx}Edit`,
    delete: `${pfx}Delete`,
  },
  calendar: {
    weekday: [`${pfx}Sunday`, `${pfx}Monday`, `${pfx}Tuesday`, `${pfx}Wednesday`, `${pfx}Thursday`, `${pfx}Friday`, `${pfx}Saturday`],
    weekdayshort: [`${pfx}MO`, `${pfx}TU`, `${pfx}WE`, `${pfx}TH`, `${pfx}FR`, `${pfx}SA`, `${pfx}SU`],
    weekdayshortsun: [`${pfx}SU`, `${pfx}MO`, `${pfx}TU`, `${pfx}WE`, `${pfx}TH`, `${pfx}FR`, `${pfx}SA`],
    monthnames: [`${pfx}January`, `${pfx}February`, `${pfx}March`, `${pfx}April`, `${pfx}May`, `${pfx}June`, `${pfx}July`, `${pfx}August`, `${pfx}September`, `${pfx}October`, `${pfx}November`, `${pfx}December`],
    monthnamesshort: [`${pfx}Jan`, `${pfx}Feb`, `${pfx}Mar`, `${pfx}Apr`, `${pfx}May`, `${pfx}Jun`, `${pfx}Jul`, `${pfx}Aug`, `${pfx}Sep`, `${pfx}Oct`, `${pfx}Nov`, `${pfx}Dec`],
  },
  ///// Apr. 09 - dátum után nem szükséges a pont, edit task:  April 03, 2017
  taskform: {
    addtitle: `${pfx}Add task`,
    edittitle: `${pfx}Edit task`,
    addtaskplaceholder: `${pfx}Add task`, // desktop footer
    tasktitleplaceholder: `${pfx}Task title`,
    titleerror: `${pfx}The title must be at least 1 character`,
    addbutton: `${pfx}ADD`,
    editbutton: `${pfx}SAVE`,
    cancel: `${pfx}Cancel`,
  },
  daypanel: {
    listempty: `${pfx}Your list<br/>is empty`,
    addhere: `${pfx}ADD TASK HERE`,
  }
};

Object.assign(String.prototype, {
  Break() {
    if (Platform.OS === 'web') {
      const strs = this.split('<br/>');
      return (<span>{strs[0]}<br />{strs[1]}</span>);
    }
    return this.split('<br/>').join('\n');
  }
});

export { Txt };
