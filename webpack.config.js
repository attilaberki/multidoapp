const webpack = require('webpack');

module.exports = {
  entry: [
    './index.desktop.js'
  ],
  output: {
    path: __dirname,
    publicPath: '/',
    filename: 'bundle.desktop.js'
  },
  module: {
    loaders: [{
      exclude: /node_modules/,
      loader: 'babel',
      query: {
        presets: ['react', 'es2015', 'stage-1']
      }
    }]
  },
  resolve: {
    extensions: ['', '.js', '.jsx'],
    alias: {
      'react-native': 'react-native-web'
    }
  },
  devServer: {
    historyApiFallback: true,
    contentBase: './'
  },
  plugins: [
    /*new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),*/
    /*new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    })*/
  ]

};
